"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
// example calling of service that's protected by jwt:
/*
curl -X GET -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjQ1IiwiZW1haWwiOiJkYW5nb3VsZXQxQGdtYWlsLmNvbSIsInJvbGUiOiJhZG1pbiIsImlhdCI6MTU0NDIxNTc2OSwiZXhwIjoxNTQ2NjM0OTY5fQ.tE7QxPuvxdRdKdp1BngLVlKR923QovILAChAz7S_Go8" http://localhost:3000/accounts
 */
const context_1 = require("@loopback/context");
const authentication_1 = require("@loopback/authentication");
const passport_jwt_1 = require("passport-jwt");
let JwtAuthVerificationProvider = class JwtAuthVerificationProvider {
    constructor(metadata) {
        this.metadata = metadata;
    }
    value() {
        // The function was not decorated, so we shouldn't attempt authentication
        if (!this.metadata) {
            return undefined;
        }
        const name = this.metadata.strategy;
        const strategyOptions = {
            jwtFromRequest: passport_jwt_1.ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: process.env.JWT_SECRET || 'MyWildSecret'
        };
        if (name === 'JwtStrategy') {
            return new passport_jwt_1.Strategy(strategyOptions, this.verify);
        }
        else {
            return Promise.reject(`The strategy ${name} is not available.`);
        }
    }
    async verify(jwtPayload, cb) {
        if (jwtPayload === undefined) {
            cb(new Error('invalid jwt token'), false);
        }
        else {
            // @ts-ignore
            let payload = {
                // @ts-ignore
                id: jwtPayload.id,
                // @ts-ignore
                email: jwtPayload.email,
                // @ts-ignore
                exp: jwtPayload.exp,
                // @ts-ignore
                iat: jwtPayload.iat,
                // @ts-ignore
                firstName: jwtPayload.firstName || undefined,
                // @ts-ignore
                lastName: jwtPayload.lastName || undefined,
                // @ts-ignore
                name: jwtPayload.name || undefined,
                // @ts-ignore
                role: jwtPayload.role
            };
            cb(null, payload);
        }
        // find user by name & password
        // call cb(null, false) when user not found
        // call cb(null, user) when user is authenticated
    }
};
JwtAuthVerificationProvider = __decorate([
    __param(0, context_1.inject(authentication_1.AuthenticationBindings.METADATA)),
    __metadata("design:paramtypes", [Object])
], JwtAuthVerificationProvider);
exports.JwtAuthVerificationProvider = JwtAuthVerificationProvider;
//# sourceMappingURL=jwt-auth-verification.provider.js.map