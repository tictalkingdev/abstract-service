/// <reference types="express" />
import { Request } from '@loopback/rest';
/**
 * An alive controller
 */
export declare class PingController {
    private req;
    constructor(req: Request);
    ping(): object;
}
