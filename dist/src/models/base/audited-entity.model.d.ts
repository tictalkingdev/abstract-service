import { Entity } from '@loopback/repository';
export declare class AuditedEntity extends Entity {
    createdAt: Date;
    updatedAt?: Date;
    constructor(data?: Partial<AuditedEntity>);
}
