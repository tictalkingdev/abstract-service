import {Entity, model, property} from '@loopback/repository';

@model()
export class AuditedEntity extends Entity {
  @property({
    postgresql: {
      columnName: 'created_at'
    },
    type: 'Date',
    required: false
  })
  createdAt: Date;

  @property({
    postgresql: {
      columnName: 'updated_at'
    },
    type: 'Date',
    required: false
  })
  updatedAt?: Date;

  constructor(data?: Partial<AuditedEntity>) {
    super(data);
  }
}
