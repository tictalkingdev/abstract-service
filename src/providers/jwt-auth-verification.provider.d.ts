import { Provider, ValueOrPromise } from '@loopback/context';
import { AuthenticationMetadata, UserProfile } from '@loopback/authentication';
import { Strategy } from 'passport-jwt';
export declare class JwtAuthProvider implements Provider<Strategy | undefined> {
    private metadata;
    constructor(metadata: AuthenticationMetadata);
    value(): ValueOrPromise<Strategy | undefined>;
    verify(jwtPayload: object, cb: (err: Error | null, user?: UserProfile | false) => void): void;
}
