import {Provider, inject, ValueOrPromise} from '@loopback/context';
import {
  AuthenticationBindings,
  AuthenticationMetadata,
  UserProfile
} from '@loopback/authentication';

import {Strategy} from 'passport';
import {Strategy as JwtStrategy, ExtractJwt} from 'passport-jwt';

export class JwtAuthVerificationProvider implements Provider<Strategy | undefined> {
  constructor(
    @inject(AuthenticationBindings.METADATA)
    private metadata: AuthenticationMetadata,
  ) {}

  value(): ValueOrPromise<Strategy | undefined> {
    // The function was not decorated, so we shouldn't attempt authentication
    if (!this.metadata) {
      return undefined;
    }

    const name = this.metadata.strategy;
    const strategyOptions = {
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: process.env.JWT_SECRET || 'MyWildSecret'
    };

    if (name === 'JwtStrategy') {
      return new JwtStrategy(strategyOptions, this.verify);
    } else {
      return Promise.reject(`The strategy ${name} is not available.`);
    }
  }

  async verify(jwtPayload: object, cb: (err: Error | null, user?: UserProfile | false) => void) {

      if (jwtPayload === undefined) {
          cb(new Error('invalid jwt token'), false);
      } else {
          // @ts-ignore
          let payload = {
              // @ts-ignore
              id: jwtPayload.id,
              // @ts-ignore
              email: jwtPayload.email,
              // @ts-ignore
              exp: jwtPayload.exp,
              // @ts-ignore
              iat: jwtPayload.iat,
              // @ts-ignore
              firstName: jwtPayload.firstName || undefined,
              // @ts-ignore
              lastName: jwtPayload.lastName || undefined,
              // @ts-ignore
              name: jwtPayload.name || undefined,
              // @ts-ignore
              role: jwtPayload.role
          } as UserProfile;

          cb(null, payload);
      }

      // find user by name & password
    // call cb(null, false) when user not found
    // call cb(null, user) when user is authenticated
  }
}
