"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const context_1 = require("@loopback/context");
const authentication_1 = require("@loopback/authentication");
const passport_jwt_1 = require("passport-jwt");
let JwtAuthProvider = class JwtAuthProvider {
    constructor(metadata) {
        this.metadata = metadata;
    }
    value() {
        // The function was not decorated, so we shouldn't attempt authentication
        if (!this.metadata) {
            return undefined;
        }
        const name = this.metadata.strategy;
        const strategyOptions = {
            jwtFromRequest: passport_jwt_1.ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: 'c6a517204cbeddc5f9108bd16a24e54fefc514236a8ec317153b9b3e548fc49b514c4c32fe2bca07b7ef09424e6af130669b6783759688b507800561fd051598'
        };
        if (name === 'JwtStrategy') {
            return new passport_jwt_1.Strategy(strategyOptions, this.verify);
        }
        else {
            return Promise.reject(`The strategy ${name} is not available.`);
        }
    }
    verify(jwtPayload, cb) {
        cb(null, { id: '1', name: 'hiya', email: 'dangoulet1@gmail.com' });
        // find user by name & password
        // call cb(null, false) when user not found
        // call cb(null, user) when user is authenticated
    }
};
JwtAuthProvider = __decorate([
    __param(0, context_1.inject(authentication_1.AuthenticationBindings.METADATA)),
    __metadata("design:paramtypes", [Object])
], JwtAuthProvider);
exports.JwtAuthProvider = JwtAuthProvider;
//# sourceMappingURL=auth-strategy.provider.js.map